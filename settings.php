<?php
if(!class_exists('EventUpon_Calendar_Settings'))
{
    class EventUpon_Calendar_Settings
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // register actions
            add_action('admin_init', array(&$this, 'admin_init'));
            add_action('admin_menu', array(&$this, 'add_menu'));

            add_option('eventupon_calendar_width', '100%');
            add_option('eventupon_calendar_height', '600px');
            add_option('eventupon_calendar_view_mode', 'agenda');
            add_option('eventupon_calendar_url', 'http://www.eventupon.com/embed/calendar');
        } // END public function __construct


        /**
         * hook into WP's admin_init action hook
         */
        public function admin_init()
        {
            // register your plugin's settings
            register_setting('eventupon_calendar-group', 'eventupon_calendar_width');
            register_setting('eventupon_calendar-group', 'eventupon_calendar_height');
/*            register_setting('eventupon_calendar-group', 'eventupon_calendar_font_size');*/
            register_setting('eventupon_calendar-group', 'eventupon_calendar_background_color');
            register_setting('eventupon_calendar-group', 'eventupon_calendar_grid_color');
            register_setting('eventupon_calendar-group', 'eventupon_calendar_header_background_color');
            register_setting('eventupon_calendar-group', 'eventupon_calendar_header_text_color');
            register_setting('eventupon_calendar-group', 'eventupon_calendar_view_mode');
            register_setting('eventupon_calendar-group', 'eventupon_calendar_saved_search_id');
            register_setting('eventupon_calendar-group', 'eventupon_calendar_activate_links');
            //register_setting('eventupon_calendar-group', 'eventupon_calendar_url');

// add General settings section
            add_settings_section(
                'eventupon_calendar-section',
                'General Settings',
                array(&$this, 'settings_section_eventupon_calendar'),
                'eventupon_calendar_general'
            );

            add_settings_field(
                'eventupon_calendar-calendar_width',
                'Calendar Width',
                array(&$this, 'settings_field_input_text'),
                'eventupon_calendar_general',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_width'
                )
            );

            add_settings_field(
                'eventupon_calendar-calendar_height',
                'Calendar Height',
                array(&$this, 'settings_field_input_text'),
                'eventupon_calendar_general',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_height'
                )
            );

            add_settings_field(
                'eventupon_calendar-calendar_view_mode',
                'Default View Mode',
                array(&$this, 'settings_field_select'),
                'eventupon_calendar_general',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_view_mode',
                    'options' => array('discovery' => 'Discovery', 'week' => 'Week', 'month' => 'Month', 'agenda' => 'Agenda'),
                )
            );

            add_settings_field(
                'eventupon_calendar-calendar_saved_search',
                'EU Search Token',
                array(&$this, 'settings_field_input_text'),
                'eventupon_calendar_general',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_saved_search_id',
                    'help' => 'Please login into <a href="http://www.eventupon.com">http://www.eventupon.com</a> , create a filter and provide the filter id here'
                )
            );

            add_settings_field(
                'eventupon_calendar-activate_links',
                'Activate EventUpon Links',
                array(&$this, 'settings_field_checkbox'),
                'eventupon_calendar_general',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_activate_links',
                    'help' => 'This must be set so that clicking on an event will link to the event details (WordPress requires all external links to be opt-in)'
                )
            );

            add_settings_field(
                'eventupon_calendar-calendar_css_file',
                'Custom CSS file, absolute url',
                array(&$this, 'settings_field_input_text'),
                'eventupon_calendar_general',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_css_file',
                    'help' => 'Ex. http://url to your css file',
                    'class' => 'eu-input-wide',
                )
            );

            add_settings_section(
                'eventupon_calendar-section',
                'Customize Colors',
                array( &$this, 'settings_section_eventupon_calendar' ),
                'eventupon_calendar_colors'
            );

            add_settings_field(
                'eventupon_calendar-calendar_background_color',
                'Background',
                array(&$this, 'settings_field_input_text'),
                'eventupon_calendar_colors',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_background_color',
                    'class' => 'color-field',
                )
            );

            add_settings_field(
                'eventupon_calendar-calendar_grid_color',
                'Grid',
                array(&$this, 'settings_field_input_text'),
                'eventupon_calendar_colors',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_grid_color',
                    'class' => 'color-field',
                )
            );

            add_settings_field(
                'eventupon_calendar-calendar_header_background_color',
                'Header Background',
                array(&$this, 'settings_field_input_text'),
                'eventupon_calendar_colors',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_header_background_color',
                    'class' => 'color-field',
                )
            );

            add_settings_field(
                'eventupon_calendar-calendar_header_text_color',
                'Header Text',
                array(&$this, 'settings_field_input_text'),
                'eventupon_calendar_colors',
                'eventupon_calendar-section',
                array(
                    'field' => 'eventupon_calendar_header_text_color',
                    'class' => 'color-field',
                )
            );
            // Possibly do additional admin_init tasks
        } // END public static function activate

        public function settings_section_eventupon_calendar()
        {
            // Think of this as help text for the section.
            echo 'These settings do things for the EventUpon Calendar.';
        }

        /**
         * This function provides text inputs for settings fields
         */
        public function settings_field_input_text($args)
        {
            // Get the field name from the $args array
            $field = $args['field'];
            // Get the value of this setting
            $value = get_option($field);
            // echo a proper input type="text"
            echo sprintf('<input type="text" name="%s" id="%s" value="%s" ' . (isset($args['class']) ? 'class="' . $args['class'] . '"' : '') . ' />', $field, $field, $value);
            if (isset($args['help'])) {
                echo '<p>' . $args['help'] . '</p>';
            }
        } // END public function settings_field_input_text($args)

        /**
         * This function provides text inputs for settings fields
         */
        public function settings_field_checkbox($args)
        {
            // Get the field name from the $args array
            $field = $args['field'];
            // Get the value of this setting
            $value = get_option($field);
            // echo a proper input type="text"
            echo sprintf('<input type="checkbox" name="%s" id="%s" value="1" ' . checked( 1, $value, false ) . (isset($args['class']) ? ' class="' . $args['class'] . '"' : '') . ' />', $field, $value);
            if (isset($args['help'])) {
                echo '<p>' . $args['help'] . '</p>';
            }
        } // END public function settings_field_input_text($args)

        /**
         * This function provides select for settings fields
         */
        public function settings_field_select($args)
        {
            // Get the field name from the $args array
            $field = $args['field'];
            $options = $args['options'];
            // Get the value of this setting
            $value = get_option($field);
            // echo a proper input type="text"
            echo sprintf('<select name="%s" id="%s" ' . (isset($args['class']) ? 'class="' . $args['class'] . '"' : '') . ' />', $field, $field);
            foreach ($options as $key => $val) {
                echo sprintf('<option value="%s" ' . ($value == $key ? 'selected="selected"' : '') . '>%s</option>', $key, $val);
            }
            echo '</select>';
        } // END public function settings_field_select($args)

        /**
         * add a menu
         */
        public function add_menu()
        {
            // Add a page to manage this plugin's settings
            add_options_page(
                'EventUpon Calendar Settings',
                'EventUpon Calendar',
                'manage_options',
                'eventupon_calendar',
                array(&$this, 'plugin_settings_page')
            );
        } // END public function add_menu()

        /**
         * Menu Callback
         */
        public function plugin_settings_page()
        {
            if(!current_user_can('manage_options'))
            {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            // Render the settings template
            include(sprintf("%s/templates/settings.php", dirname(__FILE__)));
        } // END public function plugin_settings_page()
    } // END class EventUpon_Calendar_Settings
} // END if(!class_exists('EventUpon_Calendar_Settings'))

add_action( 'admin_enqueue_scripts', 'eu_add_color_picker' );
function eu_add_color_picker( $hook ) {
    if( is_admin() ) {
        // Add the color picker css file
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_style( 'eu-admin', EVENTUPON_CALENDAR__PLUGIN_URL . '_inc/eu-admin.css');
        // Include our custom jQuery file with WordPress Color Picker dependency
        wp_enqueue_script( 'custom-script-handle', EVENTUPON_CALENDAR__PLUGIN_URL . '_inc/custom-script.js', array('jquery', 'wp-color-picker' ), false, true );

    }
}
