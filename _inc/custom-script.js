(function( $ ) {

    // Add Color Picker to all inputs that have 'color-field' class
    $(function() {
        $('input.color-field').wpColorPicker();
    });

})( jQuery );