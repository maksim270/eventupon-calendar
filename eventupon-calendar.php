<?php
/*
Plugin Name: EventUpon Calendar
Plugin URI: http://fyaconiello.github.com/wp-plugin-template
Description: Displays events from EventUpon.com, activate and place [eu-calendar] tag to a page you want to have the calendar.
Version: 1.0
Author: Max Kovrigovich
Author URI:
License: GPL2
*/
/*
Copyright 2014  Max Kovrigovich (email : max.kovrigovich@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define( 'EVENTUPON_CALENDAR__PLUGIN_URL', plugin_dir_url( __FILE__ ) );

if(!class_exists('EventUpon_Calendar'))
{
    class EventUpon_Calendar
    {
        const EU_CALENDAR_TAG = 'eu-calendar';
        const EU_CALENDAR_URL = '//www.eventupon.com/embed/calendar';
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // Initialize Settings
            require_once(sprintf("%s/settings.php", dirname(__FILE__)));
            $EventUpon_Calendar_Settings = new EventUpon_Calendar_Settings();

            $plugin = plugin_basename(__FILE__);
            add_filter("plugin_action_links_$plugin", array( $this, 'plugin_settings_link' ));

        } // END public function __construct

        /**
         * Activate the plugin
         */
        public static function activate()
        {
            // Do nothing
        } // END public static function activate

        /**
         * Deactivate the plugin
         */
        public static function deactivate()
        {
            delete_option('eventupon_calendar_width');
            delete_option('eventupon_calendar_height');
            delete_option('eventupon_calendar_background_color');
            delete_option('eventupon_calendar_grid_color');
            delete_option('eventupon_calendar_header_background_color');
            delete_option('eventupon_calendar_header_text_color');
            delete_option('eventupon_calendar_view_mode');
            delete_option('eventupon_calendar_saved_search_id');
            delete_option('eventupon_calendar_url');
            delete_option('eventupon_calendar_activate_links');
        } // END public static function deactivate

        // Add the settings link to the plugins page
        function plugin_settings_link($links)
        {
            $settings_link = '<a href="options-general.php?page=eventupon_calendar">Settings</a>';
            array_unshift($links, $settings_link);
            return $links;
        }

        public static function eu_calendar_tag_filter($content)
        {
            $width = get_option('eventupon_calendar_width', '100%');
            $height = get_option('eventupon_calendar_height', '600');
            $viewMode = get_option('eventupon_calendar_view_mode', 'agenda');
            $savedSearchId = get_option('eventupon_calendar_saved_search_id', '');
            $backgroundColor = get_option('eventupon_calendar_background_color', '#FFFFFF');
            $colorGrid = get_option('eventupon_calendar_grid_color', '#000000');
            $colorHeaderBackground = get_option('eventupon_calendar_header_background_color', '#CCCCCC');
            $colorHeaderText = get_option('eventupon_calendar_header_text_color', '#000000');
            $cssFile = get_option('eventupon_calendar_css_file', '');
            $url = get_option('eventupon_calendar_url', self::EU_CALENDAR_URL);
            $activateLinks = get_option('eventupon_calendar_activate_links', 0);

            $parameters = array(
                'view-mode=' . urlencode($viewMode),
                'color-header-text=' . urlencode($colorHeaderText),
                'color-header-bkg=' . urlencode($colorHeaderBackground),
                'color-grid=' . urlencode($colorGrid),
                'color-bkg=' . urlencode($backgroundColor),
                'css-file=' . urlencode($cssFile),
                'sid=' . $savedSearchId,
                'activate-links=' . $activateLinks,
            );
            $content = '<iframe src="' . $url .'?' . implode('&', $parameters) . '" width="' . $width .  '" height="' . $height . '" style="border:none;"></iframe>';

            return $content;
        }

    } // END class EventUpon_Calendar
} // END if(!class_exists('EventUpon_Calendar'))

if(class_exists('EventUpon_Calendar'))
{
    // Installation and uninstallation hooks
    register_activation_hook(__FILE__, array('EventUpon_Calendar', 'activate'));
    register_deactivation_hook(__FILE__, array('EventUpon_Calendar', 'deactivate'));
    add_shortcode(EventUpon_Calendar::EU_CALENDAR_TAG, array('EventUpon_Calendar', 'eu_calendar_tag_filter'));

    // instantiate the plugin class
    $eventupon_calendar = new EventUpon_Calendar();
}
