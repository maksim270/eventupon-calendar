=== EventUpon ===
Contributors: maksim270
Donate link: http://www.eventupon.com/
Tags: calendar, events, community, eventupon, embed, shortcode, plugin, ajax, events widget
Requires at least: 3.0.1
Tested up to: 4.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A calendar system for events either of your organization and/or from other organizations’ events in the EventUpon database.

== Description ==

EventUpon makes it easy for people and organization to have a calendar of events on their website whether it’s their events or events coming from other organizations that are contained in the enormous EventUpon database of events.

You can add your events to your calendar which will also be automatically published to the entire EventUpon community. You have the option to promote your events.

You can pull events from other organizations into your calendar. You can specify organizations and events from those organizations will be automatically pulled into your calendar. Or you can specify an area, using a zip code and radius, and all events within that area will be automatically pulled into your calendar. You can also curate the events and select which events are displayed in your calendar and which events aren’t.

There are four different Calendar Views. You can specify the default in WordPress settings:
Monthly
Weekly
Agenda
Discovery Stream (tiled view)

Event Details
Title and description
Address with map icon for visual map display
Sharing with Twitter, Facebook, LinkedIn, email or embed code
Link to Registration

Our community calendars were designed for mobile, tablet and desktop.

== Installation ==

1. Upload the plugin to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Optional: Change colors, See WP Admin->Plugins->EU Community Calendar->Settings
4. Place the tag [eu-calendar] in your templates

= Once Installed =

You need to create an account and set up your filter on EventUpon.com. Your filter specifies what organizations’ events you want displayed in your calendar.

* First – Go to [link](https://www.EventUpon.com) and create an Organization EventUpon Account and specify your organization. EventUpon has thousands of organizations so you can search and see if it all dy exists in EventUpon. If not, simply add your organization.
* Second – In EventUpon, click Menu, My Dashboard and Install Community Calendar.
* Third – If you will only want your organization’s events in the calendar, then you can skip this step. If you want additional events you’ll need to setup your event filter to select which events you want displayed in your instance of the EventUpon Community Calendar.  Choose by organizations or location.
* Fourth – Press the button to generate the EventUpon code that gets inserted into your WordPress plugin.

= Creating Events for your Calendar =

Adding your events to your calendar is easy:

* You already created an EventUpon account and claimed your organization as part of the Install process.
* In EventUpon, click Menu -> My Dashboard -> +Add Event
* A popup with a choice of three different tools will appear. If you use EventUpon the event will immediately be available to view in EventUpon. If you use an event registration tool the event will usually be available to view in EventUpon within 24 hours.

== Frequently Asked Questions ==

= Can I use my existing WordPress theme? =

Yes! EventUpon works out-of-the-box with nearly every WordPress theme.

= Can I change the list of Organizations Events to be displayed? =

Yes, follow these steps:

* Add or remove orgs that will be displayed on your Calendar
* Click on Menu -> My Dashboard -> Modify Community Calendar
* Click on Upgrade/Downgrade
* Make the changes to your subscription:
Add: click on Add New Organization, type in the beginning of the org name and select the organization
Remove: click on the X next to the org name
* Click Confirm

= Will this work on WordPress multisite? =

Yes! If your WordPress installation has multisite enabled, EventUpon will work.

= Where can I get support? =

EventUpon provides free support by emailing support@eventupon.com

For dedicated consultations, email info@eventupon.com

= Where can I report a bug? =

Report bugs and suggest ideas by emailing support@eventupon.com

For the complete EventUpon FAQ [link](https://docs.google.com/document/d/1PL8vhfnyFDgZgq92GalAQEw5OuFD1UtgK-c4F6APa2E/edit)!

== Screenshots ==

1. **Installed Community Calendar at an EventUpon client CampusPhilly.**
2. **Expanded Event**

== Changelog ==

= 0.1 =

Initial upload

== Upgrade Notice ==

= 0.1 =

Initial install

== Dependencies ==
jQuery
