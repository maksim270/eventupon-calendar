<div class="wrap">
    <h2>EventUpon Calendar</h2>
    <form method="post" action="options.php">
        <?php @settings_fields('eventupon_calendar-group'); ?>
        <?php @do_settings_fields('eventupon_calendar-group'); ?>

        <?php do_settings_sections('eventupon_calendar_general'); ?>
	    <?php do_settings_sections('eventupon_calendar_colors'); ?>
<!--        <hr>
        <?php //do_settings_sections('eventupon_calendar_day'); ?>
        <hr>
        <?php //do_settings_sections('eventupon_calendar_week'); ?>
        <hr>
        <?php ///do_settings_sections('eventupon_calendar_month'); ?>
-->
        <?php @submit_button(); ?>
    </form>
</div>